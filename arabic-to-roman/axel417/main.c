#include <stdlib.h>
#define p printf
#define u(n, t, f, o) if (n > 8) p(o t); else if (n == 4) p(o f); else {if (n / 5) p(f); for (n %= 5;n;n--) p(o);}

int main(int _, char** v) {
	long a = atol(v[1]), M, C, X, I;
	M = a / 1000;
	a %= 1000;
	C = a / 100;
	a %= 100;
	X = a / 10;
	I = a % 10;
	for(;M;M--) p("M");
	u(C, "M", "D", "C")
	u(X, "C", "L", "X")
	u(I, "X", "V", "I");
}
